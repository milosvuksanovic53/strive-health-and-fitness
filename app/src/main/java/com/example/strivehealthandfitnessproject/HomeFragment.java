package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class HomeFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Button timerbtn = (Button) view.findViewById(R.id.timerbtn);
        Button exercisebtn = (Button) view.findViewById(R.id.exercisebtn);
        Button contactusbtn = (Button) view.findViewById(R.id.contactusbtn);
        Button aboutusbtn = (Button) view.findViewById(R.id.aboutusbtn);


        //now we set our onclick listenener..
        timerbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new TimerFragment()).commit();
            }
        });

        //now we set our onclick listenener..
        exercisebtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExercisesFragment()).commit();
            }
        });

        //now we set our onclick listenener..
        contactusbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
            }
        });

        //now we set our onclick listenener..
        aboutusbtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUsFragment()).commit();
            }
        });
        return view;
    }

}
