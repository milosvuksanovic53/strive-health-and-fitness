package com.example.strivehealthandfitnessproject;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

public class abs_exercise_page extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Dialog myDialog;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_abs_exercise_page);
        myDialog = new Dialog(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //what this If statement does is,
        //it ensures that the main page on reload will be the home screen at all times.



        if (savedInstanceState == null) {

            //Toast.makeText(this, "Test.....", Toast.LENGTH_SHORT).show();

            //Intent intent = new Intent (this, abs_exercises_page.class);
            //startActivity(intent);


            //setContentView(R.layout.activity_abs_exercise_page);

            //myDialog = new Dialog(this);
            //myDialog.setContentView(R.layout.activity_abs_exercise_page);
            //myDialog.show();
        }



        //Toolbar myToolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(myToolbar);
    }






    public void sit_up_popup(View view)
    {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.abs_one_popup);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void barbell_ab_rollout_popup(View view)
    {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.ab_rollout);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void vsit_crossjab_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.barbell_vsit);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void barbell_side_bend_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.barbell_sidebend);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void sledgehammer_swings_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.sledgehammer_swings);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void bench_sit_up_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.bench_sit_up);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void smith_machine_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.smith_machine);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void ab_crunch_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.ab_crunch_machine);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void knee_hip_raise_popup(View view)
    {
        TextView dismissPopup;

        myDialog.setContentView(R.layout.knee_hip_raise);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("fragmentID", item.getItemId());
        startActivity(intent);

        /*switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
            case R.id.nav_about_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUsFragment()).commit();
                break;
            case R.id.nav_documentation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DocumentationFragment()).commit();
                break;
            case R.id.nav_exercises:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExercisesFragment()).commit();
                break;
            case R.id.nav_stopwatch:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TimerFragment()).commit();
                break;
            case R.id.nav_diet:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DietsFragment()).commit();
                break;
            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                break;
            case R.id.nav_log_out:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LogOutFragment()).commit();
                break;
        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

}
