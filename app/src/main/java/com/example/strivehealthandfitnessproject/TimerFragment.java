package com.example.strivehealthandfitnessproject;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.text.Layout;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.LinearLayout;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class TimerFragment extends Fragment {

    Button btnStart, btnPause, btnReset;
    TextView txtTimer;
    Handler customHandler = new Handler();
    LinearLayout container;

    long startTime=0L,timeInMilliseconds=0L,timeSwapBuff=0L,updateTime=0L;

    Runnable updateTimerThread = new Runnable() {
        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis()-startTime;
            updateTime = timeSwapBuff+timeInMilliseconds;
            int secs = (int) (updateTime/1000);
            int mins = secs/60;
            secs%=60;
            int milliseconds=(int)(updateTime%1000);
            txtTimer.setText(""+mins+":"+String.format("%02d",secs)+":"+String.format("%03d", milliseconds));
            customHandler.postDelayed(this,0);
        }
    };

    //private Chronometer chronometer;
    //private boolean running;
    //private long pauseOffSet;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_timer, container, false);

        btnStart = (Button) view.findViewById(R.id.btnStart);
        btnPause = (Button) view.findViewById(R.id.btnPause);
        btnReset = (Button) view.findViewById(R.id.btnReset);
        txtTimer = (TextView) view.findViewById(R.id.timerValue);
        container = view.findViewById(R.id.container);

        btnPause.setEnabled(false);
        btnReset.setEnabled(false);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread,0);
                btnReset.setEnabled(false);
                btnPause.setEnabled(true);
                btnStart.setEnabled(false);
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeSwapBuff+=timeInMilliseconds;

                customHandler.removeCallbacks(updateTimerThread);
                btnReset.setEnabled(true);
                btnStart.setText("Continue");
                btnPause.setEnabled(false);
                btnStart.setEnabled(true);
            }
        });


        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtTimer.setText("0:00:000");
                SystemClock.setCurrentTimeMillis(0);

                startTime=0L;
                timeInMilliseconds=0L;
                timeSwapBuff=0L;
                updateTime=0L;

                btnStart.setText("Start");
                btnPause.setEnabled(false);
            }
        });

        /*chronometer = view.findViewById(R.id.chronometer);
        chronometer.setFormat("Time: %s");
        chronometer.setBase(SystemClock.elapsedRealtime());

        Button startChronoButton = view.findViewById(R.id.startChronoButton);
        Button pauseChronoButton = view.findViewById(R.id.pauseChronoButton);
        Button resetChronoButton = view.findViewById(R.id.resetChronoButton);

        chronometer.setFormat("00:%s");
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            public void onChronometerTick(Chronometer c) {
                long elapsedMillis = SystemClock.elapsedRealtime() -c.getBase();
                if(elapsedMillis > 3600000L){
                    c.setFormat("0%s");
                }else{
                    c.setFormat("00:%s");
                }
            }
        });

        startChronoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //check if running
                if(!running)
                {
                    chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffSet);
                    chronometer.start();
                    running = true;
                }
            }
        });


        pauseChronoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if it is running, we will be able to pause  it
                if(running)
                {
                    chronometer.stop();
                    pauseOffSet = SystemClock.elapsedRealtime() - chronometer.getBase();
                    running = false;
                }


            }
        });


        resetChronoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chronometer.setBase(SystemClock.elapsedRealtime());
                pauseOffSet = 0;
            }
        });

         */

        return view;


    }


}
