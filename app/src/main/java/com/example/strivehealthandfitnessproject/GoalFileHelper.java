package com.example.strivehealthandfitnessproject;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GoalFileHelper {

    public static final String FILENAME = "listinfo.dat";

    public static void writeGoals(ArrayList<String> goals, Context context){

        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream ops = new ObjectOutputStream(fos);
            ops.writeObject(goals);
            ops.close();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> readGoals(Context context){
        ArrayList<String> goalsList = null;
        try {
                FileInputStream fis = context.openFileInput(FILENAME);
                ObjectInputStream ois = new ObjectInputStream(fis);
                goalsList = (ArrayList<String>) ois.readObject();

            } catch (IOException e) {
                e.printStackTrace();

                goalsList = new ArrayList<>();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        return goalsList;
    }
}
