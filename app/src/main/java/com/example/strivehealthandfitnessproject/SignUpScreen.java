package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpScreen extends AppCompatActivity {

    databaseHelper db;

    EditText mTextUsername;
    EditText mTextEmail;
    EditText mTextPassword;
    EditText mTextCnfPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);

        db = new databaseHelper(this);

        mTextUsername = (EditText)findViewById(R.id.input_username);
        mTextEmail = (EditText)findViewById(R.id.input_password);
        mTextPassword = (EditText)findViewById(R.id.input_password);
        mTextCnfPassword = (EditText)findViewById(R.id.input_cnf_password);



    }

    public void SignUpButton(View view) {

        String user = mTextUsername.getText().toString().trim();
        String pwd = mTextPassword.getText().toString().trim();
        String cnf_pwd = mTextCnfPassword.getText().toString().trim();

        if(pwd.equals(cnf_pwd)){
            long val = db.addUser(user, pwd);
            if(val > 0){
                Toast.makeText(SignUpScreen.this, "You have registered", Toast.LENGTH_SHORT).show();
                Intent moveToLogin = new Intent(SignUpScreen.this, loginScreen.class);
                startActivity(moveToLogin);
            }
        }
        else{
            Toast.makeText(SignUpScreen.this, "Registration Error",Toast.LENGTH_LONG).show();
        }
    }

    public void loginButton(View view) {
        Intent intent = new Intent(this, loginScreen.class);
        startActivity(intent);
    }
}
