package com.example.strivehealthandfitnessproject;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class GoalsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private EditText goalsEditText;
    private Button addButton;
    private ListView goalsList;

    //this is an array which will hold our goals
    private ArrayList<String> ourGoals;

    //this array adapter helps fill in the listview
    private ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_goals, container, false);


        //variables we reference in our XML
        goalsEditText = view.findViewById(R.id.item_edit_text);
        addButton = view.findViewById(R.id.add_Goal);
        goalsList = view.findViewById(R.id.goals_List);

        ourGoals = GoalFileHelper.readGoals(getContext());

        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, ourGoals);

        goalsList.setAdapter(adapter);

        goalsList.setOnItemClickListener(this);

        //add your goal to the list
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(goalsEditText.length() < 1)
                {
                    Toast.makeText(getContext(), "There needs to be something to add..", Toast.LENGTH_SHORT).show();

                }else{
                    switch (v.getId()){
                        case R.id.add_Goal:
                            String goalEntered = goalsEditText.getText().toString();
                            adapter.add(goalEntered);
                            goalsEditText.setText("");

                            GoalFileHelper.writeGoals(ourGoals,getContext());

                            Toast.makeText(getContext(), "Goal Added: " + "'" + goalEntered+ "'", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

            }
        });

        return view;

    }

    //this method works as follows
    //when an item is on the list to be able to remove it, simply press the item then the toast will show
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String goalEntered = goalsEditText.getText().toString();
        ourGoals.remove(position);
        adapter.notifyDataSetChanged();
        GoalFileHelper.writeGoals(ourGoals, getContext());
        Toast.makeText(getContext(), "Goal Removed", Toast.LENGTH_SHORT).show();

    }
}
