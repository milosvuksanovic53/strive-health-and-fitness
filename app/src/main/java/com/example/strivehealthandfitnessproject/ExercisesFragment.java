package com.example.strivehealthandfitnessproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ExercisesFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_exercises, container, false);

        //our weights button here mate.
       Button weightsButton = (Button) view.findViewById(R.id.weightsbtn);
       Button cardioButton = (Button) view.findViewById(R.id.cardiobtn);

       //now we set our onclick listenener..
       weightsButton.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
               Intent in = new Intent(getActivity(), weightsPage.class);
               startActivity(in);
           }


       });

        cardioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), cardioPage.class);
                startActivity(in);
            }
        });

        return view;

    }


}
