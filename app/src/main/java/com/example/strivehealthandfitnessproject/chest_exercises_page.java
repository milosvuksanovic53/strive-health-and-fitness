package com.example.strivehealthandfitnessproject;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class chest_exercises_page extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Dialog myDialog;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chest_exercises_page);
        myDialog = new Dialog(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    public void barbell_bench_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.barbell_bench);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void incline_bench_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.incline_barbell_bench);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void flat_bench_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.flat_bench_dumbbell_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void machine_bench_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.machine_bench_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void dumbbell_pullover_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.dumbbell_pullover);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void dips_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.dips_chest);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void incline_decline_pushup_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.incline_pushup);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void cable_crossover_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.cable_crossover_machine);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void butterfly_chest_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.butterfly_chest_workout);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("fragmentID", item.getItemId());
        startActivity(intent);

        /*switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
            case R.id.nav_about_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUsFragment()).commit();
                break;
            case R.id.nav_documentation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DocumentationFragment()).commit();
                break;
            case R.id.nav_exercises:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExercisesFragment()).commit();
                break;
            case R.id.nav_stopwatch:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TimerFragment()).commit();
                break;
            case R.id.nav_diet:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DietsFragment()).commit();
                break;
            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                break;
            case R.id.nav_log_out:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LogOutFragment()).commit();
                break;
        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }
}
