package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class loginScreen extends AppCompatActivity {

    EditText mTextUsername;
    EditText mTextPassword;
    Button mButtonLogin;
    databaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        db = new databaseHelper(this);
        mTextUsername = (EditText)findViewById(R.id.input_username);
        mTextPassword = (EditText)findViewById(R.id.input_password);
        mButtonLogin = (Button)findViewById(R.id.btn_login);
    }



    public void loginButton(View view) {

        String user = mTextUsername.getText().toString().trim();
        String pwd = mTextPassword.getText().toString().trim();
        Boolean res = db.checkUser(user, pwd);

        if(res == true){
            Toast.makeText(loginScreen.this, "Successfully Logged In", Toast.LENGTH_SHORT).show();
           // getSupportFragmentManager().beginTransaction().replace(android.R.id.content, new HomeFragment()).commit();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("fragmentID", R.id.nav_home);
            startActivity(intent);

        }
        else
            Toast.makeText(loginScreen.this, "Login Error", Toast.LENGTH_SHORT).show();
    }

    public void signUp_link(View view) {

        Intent intent = new Intent(this, SignUpScreen.class);
        startActivity(intent);

    }
}
