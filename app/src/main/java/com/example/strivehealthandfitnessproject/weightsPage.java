package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class weightsPage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //on the weights page now consisting of multiple btns.
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weights_page);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();


    }

    //when we press on the first btn being 'back'
    // it will lead onto the back_exerecises_page.
    public void backbtn(View view) {
        Intent intent = new Intent(this, back_exercises_page.class);
        startActivity(intent);
    }

    //pressing on the 2nd option 'Chest'
    //will lead into the chest_exercises_page.
    public void chestbtn(View view) {
        Intent intent = new Intent(this, chest_exercises_page.class);
        startActivity(intent);
    }

    //pressing on the 3rd option 'shoulder'
    //it will lead onto the shoulder_exercises_page
    public void shouldersbtn(View view) {
        Intent intent = new Intent(this, shoulder_exercises_page.class);
        startActivity(intent);
    }
    //pressing on the 4th option 'arms'
    //will lead into the arm_exercises_page
    public void armsbtn(View view) {
        Intent intent = new Intent(this, arm_exercises_page.class);
        startActivity(intent);
    }

    //pressing on the 5th and final option 'abs'
    //will lead into the abs_exercise_page.
    public void absbtn(View view) {
        //TESTTTTTTTT
        Intent intent = new Intent(this, abs_exercise_page.class);
       // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        Toast.makeText(this, "test test", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("fragmentID", item.getItemId());
        startActivity(intent);

        /*switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
            case R.id.nav_about_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUsFragment()).commit();
                break;
            case R.id.nav_documentation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DocumentationFragment()).commit();
                break;
            case R.id.nav_exercises:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExercisesFragment()).commit();
                break;
            case R.id.nav_stopwatch:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TimerFragment()).commit();
                break;
            case R.id.nav_diet:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DietsFragment()).commit();
                break;
            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                break;
            case R.id.nav_log_out:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LogOutFragment()).commit();
                break;
        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }

    }
}
