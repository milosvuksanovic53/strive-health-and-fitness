package com.example.strivehealthandfitnessproject;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class shoulder_exercises_page extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoulder_exercises_page);
        myDialog = new Dialog(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }

    public void side_laterals_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.side_laterals);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void front_incline_dumbell_press_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.front_incline_dumbbell_raise);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void standing_dumbbell_press_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.standing_dumbbell_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void seated_barbbell_military_press_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.seated_barbell_military_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void leverage_shoulder_press_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.leverage_shoulder_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void external_rotation_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.external_rotation);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    public void clean_press_popup(View view) {

        TextView dismissPopup;

        myDialog.setContentView(R.layout.clean_press);
        myDialog.show();

        dismissPopup = myDialog.findViewById(R.id.dismissPopup);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("fragmentID", item.getItemId());
        startActivity(intent);

        /*switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                break;
            case R.id.nav_about_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutUsFragment()).commit();
                break;
            case R.id.nav_documentation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DocumentationFragment()).commit();
                break;
            case R.id.nav_exercises:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ExercisesFragment()).commit();
                break;
            case R.id.nav_stopwatch:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TimerFragment()).commit();
                break;
            case R.id.nav_diet:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DietsFragment()).commit();
                break;
            case R.id.nav_contact_us:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ContactUsFragment()).commit();
                break;
            case R.id.nav_log_out:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LogOutFragment()).commit();
                break;
        }*/
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }

    }
}
