package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class DocumentationFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_documentation, container, false);

        //our buttons here mate.
        Button cvdButton = (Button) view.findViewById(R.id.cvdbtn);
        Button moraleButton = (Button) view.findViewById(R.id.moralebtn);
        Button illnessButton = (Button) view.findViewById(R.id.illnessbtn);

        //now we set our onclick listenener..
        cvdButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent in = new Intent(getActivity(), cvdPage.class);
                startActivity(in);
            }
        });

        //when we press the 2nd option on the documentation page it will lead onto
        //the morale page.
        moraleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), moralePage.class);
                startActivity(in);
            }
        });

        //when we press the 3rd option on the documentation page
        //it will lead into the illness page.
        illnessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(), illnessPage.class);
                startActivity(in);
            }
        });
        return view;
    }
}
