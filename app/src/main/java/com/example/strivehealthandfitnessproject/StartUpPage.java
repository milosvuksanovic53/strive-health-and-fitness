package com.example.strivehealthandfitnessproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StartUpPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up_page);
    }

    public void registerButton(View view) {

        Intent intent = new Intent(this, SignUpScreen.class);
        startActivity(intent);
    }

    public void LoginButton(View view) {

        Intent intent = new Intent(this, loginScreen.class);
        startActivity(intent);
    }
}
